# TP2 pt. 1 : Gestion de service
# Sommaire

- [TP2 pt. 1 : Gestion de service](#tp2-pt-1--gestion-de-service)
- [Sommaire](#sommaire)
- [0. Prérequis](#0-prérequis)
  - [Checklist](#checklist)
- [I. Un premier serveur web](#i-un-premier-serveur-web)
  - [1. Installation](#1-installation)
  - [2. Avancer vers la maîtrise du service](#2-avancer-vers-la-maîtrise-du-service)
- [II. Une stack web plus avancée](#ii-une-stack-web-plus-avancée)
  - [1. Intro](#1-intro)
  - [2. Setup](#2-setup)
    - [A. Serveur Web et NextCloud](#a-serveur-web-et-nextcloud)
    - [B. Base de données](#b-base-de-données)
    - [C. Finaliser l'installation de NextCloud](#c-finaliser-linstallation-de-nextcloud)

# I. Un premier serveur web

## 1. Installation

🖥️ **VM web.tp2.linux**

| Machine         | IP            | Service                 | Port ouvert | IP autorisées |
|-----------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux` | `10.102.1.11` | Serveur Web             | ?           | ?             |

> Ce tableau devra figurer à la fin du rendu, avec les ? remplacés par la bonne valeur (un seul tableau à la fin). Je vous le remets à chaque fois, à des fins de clarté, pour lister les machines qu'on a à chaque instant du TP.

🌞 **Installer le serveur Apache**
```
# Paquet httpd
[glama@web ~]$ sudo yum install httpd
``` 

🌞 **Démarrer le service Apache**

```
# Démarrage service httpd

[glama@web ~]$ sudo httpd -k start

# Démarrer httpd au démarrage 

[glama@web ~]$ sudo systemctl enable httpd
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.

#### Port d'écoute apache

[glama@web ~]$ sudo ss -alnpt
[sudo] password for glama:
State           Recv-Q          Send-Q                    Local Address:Port                     Peer Address:Port          Process
[...]
LISTEN          0               128                                   *:80                                  *:*              users:(("httpd",pid=864,fd=4),("httpd",pid=863,fd=4),("httpd",pid=862,fd=4),("httpd",pid=835,fd=4))

#### Ouverture du port 80 en TCP sur le firewall

[glama@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[glama@web ~]$ sudo firewall-cmd --reload
success
[glama@web ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: ssh
  ports: 80/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

🌞 **TEST**

```

### Check si il démarre automatiquement
[glama@web ~]$ systemctl is-enabled httpd
enabled

## Check si il est démarré
[glama@web ~]$ systemctl is-active httpd
active

### Curl localhost 

[glama@web ~]$ curl localhost
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
[...]

#### Vérif sur le PC que le serveur web est accessible

C:\Users\guigu> curl 10.102.1.11
curl : HTTP Server Test Page
This page is used to test the proper operation of an HTTP server after it has been installed on a Rocky Linux system. If
you can read this page, it means that the software it working correctly.
Just visiting?
[...]
```

## 2. Avancer vers la maîtrise du service

🌞 **Le service Apache...**

```
### Commande pour activer apache quand la machine s'allume

[glama@web ~]$ sudo systemctl enable httpd

###Preuve que le service est actif au démarage 

[glama@web ~]$ systemctl status httpd

### Afficher contenu httpd.service 

[glama@web ~]$ systemctl cat httpd
# /usr/lib/systemd/system/httpd.service
# See httpd.service(8) for more information on using the httpd service.

# Modifying this file in-place is not recommended, because changes
# will be overwritten during package upgrades.  To customize the
[...]
```

🌞 **Déterminer sous quel utilisateur tourne le processus Apache**

```
### Affichage de l'user apache

[glama@web ~]$ cd /etc/httpd/
[glama@web httpd]$ cd conf
[glama@web conf]$ ls
httpd_backup.conf  httpd.conf  magic
[glama@web conf]$ vi httpd.conf

[...]

User apache

[...]

## Affichage pq -ef

[glama@web conf]$ ps -ef
UID          PID    PPID  C STIME TTY          TIME CMD
[...]
apache       861     835  0 14:57 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache       862     835  0 14:57 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache       863     835  0 14:57 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache       864     835  0 14:57 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
[...]


### Vérif des droits d'accès de la page d'accueil

[glama@web conf]$ cd /usr/share/testpage/
[glama@web testpage]$ ls
index.html
[glama@web testpage]$ ls -al index.html
-rw-r--r--. 1 root root 7621 Jun 11 17:23 index.html

#### Il a donc accès en lecture a cette page
```

🌞 **Changer l'utilisateur utilisé par Apache**

```
### Créer nouvel user 

[glama@web ~]$ sudo useradd zebi -d /usr/share/httpd/zebi -s /sbin/nologin
useradd: warning: the home directory already exists.
Not copying any file from skel directory into it.
Creating mailbox file: File exists
[glama@web ~]$ sudo usermod -aG apache zebi


### Modif conf apache
[glama@web ~]$ sudo vi /etc/httpd/conf/httpd.conf

[...]

User zebi

[...]

### redémarrage Apache

[glama@web ~]$ sudo systemctl restart httpd

## Vérif

[glama@web ~]$ ps -ef
[...]
zebi        2086    2084  0 15:38 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
zebi        2087    2084  0 15:38 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
zebi        2088    2084  0 15:38 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
zebi        2089    2084  0 15:38 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
[...]

```

🌞 **Faites en sorte que Apache tourne sur un autre port**

```
## Modif config apache

[glama@web ~]$ sudo vi /etc/httpd/conf/httpd.conf
[...]
Listen 5555
[...]

### Ouverture port firewall et fermeture ancien

[glama@web ~]$ sudo firewall-cmd --remove-port=80/tcp --permanent
success
[glama@web ~]$ sudo firewall-cmd --add-port=5555/tcp --permanent
success
[glama@web ~]$ sudo firewall-cmd --reload
success
[glama@web ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: ssh
  ports: 5555/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:

### Redémarrage Apache

[glama@web ~]$ sudo systemctl restart httpd

### Vérif commande ss

[glama@web ~]$ sudo ss -alnpt
[...]
LISTEN          0               128                                   *:5555                                *:*
 users:(("httpd",pid=2610,fd=4),("httpd",pid=2609,fd=4),("httpd",pid=2608,fd=4),("httpd",pid=2605,fd=4))
[...]

### Curl localhost

[glama@web ~]$ curl localhost:5555
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/
[...]


### Vérif sur PC 

C:\Users\guigu> curl 10.102.1.11:5555
curl : HTTP Server Test Page
This page is used to test the proper operation of an HTTP server after it has been installed on a Rocky Linux system. If
you can read this page, it means that the software it working correctly.
Just visiting?
[...]
```


📁 **Fichier `/etc/httpd/conf/httpd.conf`** 

# II. Une stack web plus avancée


## 2. Setup

🖥️ **VM db.tp2.linux**

| Machine         | IP            | Service                 | Port ouvert | IP autorisées |
|-----------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux` | `10.102.1.11` | Serveur Web             | 80          |               |
| `db.tp2.linux`  | `10.102.1.12` | Serveur Base de Données | 3306        | 10.102.1.11   |


### A. Serveur Web et NextCloud

**Créez les 2 machines et déroulez la [📝**checklist**📝](#checklist).**

🌞 Install du serveur Web et de NextCloud sur `web.tp2.linux`

```

### Install de EPEL
[glama@web ~]$ sudo dnf install epel-release

### Update des repos
[glama@web ~]$ sudo dnf update

### Install de REMI
[glama@web ~]$sudo dnf install -y https://rpms.remirepo.net/enterprise/remi-release-8.rpm

###Changement version REMI
[glama@web ~]$ sudo dnf module enable php:remi-7.4

### Install de tout les package nécessaire
[glama@web ~]$ dnf install -y httpd mariadb-server vim wget zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp
  230  sudo dnf install -y httpd mariadb-server vim wget zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp
  
### Activation httpd
[glama@web ~]$ systemctl enable httpd

### Création des dossiers nécessaire
[glama@web ~]$ sudo mkdir /etc/httpd/sites-available
[glama@web ~]$ sudo mkdir /etc/httpd/sites-enabled
[glama@web ~]$ sudo mkdir /var/www/sub-domains/

### Modif de la conf
[glama@web ~]$ vi /etc/httpd/conf/httpd.conf

'Include /etc/httpd/sites-enabled'

### Install de PHP
[glama@web ~]$ sudo dnf install httpd php

### Config de la page web 
[glama@web ~]$ sudo vi /etc/httpd/sites-available/web.tp2.linux

### Ajout d'un lien entre les 2 dossier du site
[glama@web ~]$ ln -s /etc/httpd/sites-available/web.tp2.linux /etc/httpd/sites-enabled/

### Création du dossier ou sera déposé Nextcloud
[glama@web ~]$ sudo mkdir -p /var/www/sub-domains/web.tp2.linux/html

### Config PHP (Date et heure et région)
[glama@web ~]$ timedatectl
[glama@web ~]$ vi /etc/opt/remi/php74/php.ini
[glama@web ~]$ sudo vi /etc/opt/remi/php74/php.ini
[glama@web ~]$ ls -al /etc/localtime

### Téléchargement config NextCloud
[glama@web ~]$ wget https://download.nextcloud.com/server/releases/nextcloud-22.2.0.zip
[glama@web ~]$ unzip nextcloud-22.2.0.zip
[glama@web ~]$ sudo rm nextcloud-22.2.0.zip
[glama@web ~]$cd nextcloud/
[glama@web ~]$ sudo cp -Rf * /var/www/sub-domains/web.tp2.linux/html/

### Changer propriétaire dossier pour donner droit a apache
[glama@web ~]$ sudo chown -Rf apache.apache /var/www/sub-domains/com.yourdomain.nextcloud/html

### Démarrage NextCloud
[glama@web ~]$ sudo systemctl restart httpd
```

📁 **Fichier `/etc/httpd/conf/httpd.conf`** (que vous pouvez renommer si besoin, vu que c'est le même nom que le dernier fichier demandé)
📁 **Fichier `/etc/httpd/conf/sites-available/web.tp2.linux`**

### B. Base de données

🌞 **Install de MariaDB sur `db.tp2.linux`**

```
###Install de MariaDB
[glama@db ~]$ sudo dnf install mariadb-server

### Activation au démarage de MariaDB
[glama@db ~]$ sudo systemctl enable mariadb

### Lancement de MariaDB
[glama@db ~]$ sudo systemctl start mariadb

### Config de MariaDB
[glama@db ~]$ mysql_secure_installation

### SS pour trouver sur quel port il tourne
[glama@db ~]$ sudo ss -alnpt
State     Recv-Q    Send-Q        Local Address:Port         Peer Address:Port    Process
[...]
LISTEN    0         80                        *:3306                    *:*        users:(("mysqld",pid=5496,fd=21))

------- Il tourne sur le port 3306 ------


```

🌞 **Préparation de la base pour NextCloud**

```
### Connexion a la bdd

[glama@db ~]$ sudo mysql -u root --password
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
[...]
```

```sql
MariaDB [(none)]> CREATE USER 'nextcloud'@'10.102.1.11' IDENTIFIED BY 'meow';
Query OK, 0 rows affected (0.001 sec)

MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.001 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.102.1.11';
Query OK, 0 rows affected (0.000 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.000 sec)
```


🌞 **Exploration de la base de données**

```
### On ouvre un port pour autoriser la connexion
[glama@db ~]$ sudo firewall-cmd --add-port=3306/tcp --permanent
[glama@db ~]$ sudo firewall-cmd --reload

### On essaie de se conencter depuis web.tp2.linux
[glama@web ~]$ mysql -u nextcloud -h 10.102.1.12 --password
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
[...]
```
- utilisez les commandes SQL fournies ci-dessous pour explorer la base

```sql
MariaDB [(none)]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| nextcloud          |
+--------------------+
MariaDB [(none)]> USE nextcloud;
Database changed
MariaDB [nextcloud]> SHOW TABLES;
Empty set (0.001 sec)    

```

- trouver une commande qui permet de lister tous les utilisateurs de la base de données

```sql
### Obligé d être en root sur la machine local

MariaDB [(none)]> select user,host from mysql.user;
+-----------+-------------+
| user      | host        |
+-----------+-------------+
| nextcloud | 10.102.1.11 |
| root      | 127.0.0.1   |
| root      | ::1         |
| root      | localhost   |
+-----------+-------------+
4 rows in set (0.001 sec)
```

### C. Finaliser l'installation de NextCloud

🌞 sur votre PC

- modifiez votre fichier `hosts` (oui, celui de votre PC, de votre hôte)
```
C:\Windows\system32> notepad.exe C:\Windows\System32\drivers\etc\hosts

#
127.0.0.1 localhost
::1 localhost
10.102.1.11 web.tp2.linux

```
- avec un navigateur, visitez NextCloud à l'URL `http://web.tp2.linux`
```
C:\Users\guigu> curl web.tp2.linux


StatusCode        : 200
StatusDescription : OK
Content           : <!DOCTYPE html>
                    <html>
                    <head>
                        <script> window.location.href="index.php"; </script>

```
- on va vous demander un utilisateur et un mot de passe pour créer un compte admin
  - ne saisissez rien pour le moment
- cliquez sur "Storage & Database" juste en dessous
  - choisissez "MySQL/MariaDB"
  - saisissez les informations pour que NextCloud puisse se connecter avec votre base
- saisissez l'identifiant et le mot de passe admin que vous voulez, et validez l'installation

🌞 **Exploration de la base de données**

- connectez vous en ligne de commande à la base de données après l'installation terminée
```
[glama@web ~]$ mysql -u nextcloud -h 10.102.1.12 --password
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
[...]
```

- déterminer combien de tables ont été crées par NextCloud lors de la finalisation de l'installation
  - ***bonus points*** si la réponse à cette question est automatiquement donnée par une requête SQL
```
MariaDB [nextcloud]> SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = 'BASE TABLE';
+----------+
| COUNT(*) |
+----------+
|       87 |
+----------+
1 row in set (0.006 sec)

```
