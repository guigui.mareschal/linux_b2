# TP2 pt. 2 : Maintien en condition opérationnelle

# Sommaire

- [TP2 pt. 2 : Maintien en condition opérationnelle](#tp2-pt-2--maintien-en-condition-opérationnelle)
- [Sommaire](#sommaire)
- [0. Prérequis](#0-prérequis)
  - [Checklist](#checklist)
- [I. Monitoring](#i-monitoring)
  - [1. Le concept](#1-le-concept)
  - [2. Setup](#2-setup)
- [II. Backup](#ii-backup)
  - [1. Intwo bwo](#1-intwo-bwo)
  - [2. Partage NFS](#2-partage-nfs)
  - [3. Backup de fichiers](#3-backup-de-fichiers)
  - [4. Unité de service](#4-unité-de-service)
    - [A. Unité de service](#a-unité-de-service)
    - [B. Timer](#b-timer)
    - [C. Contexte](#c-contexte)
  - [5. Backup de base de données](#5-backup-de-base-de-données)
  - [6. Petit point sur la backup](#6-petit-point-sur-la-backup)
- [III. Reverse Proxy](#iii-reverse-proxy)
  - [1. Introooooo](#1-introooooo)
  - [2. Setup simple](#2-setup-simple)
  - [3. Bonus HTTPS](#3-bonus-https)
- [IV. Firewalling](#iv-firewalling)
  - [1. Présentation de la syntaxe](#1-présentation-de-la-syntaxe)
  - [2. Mise en place](#2-mise-en-place)
    - [A. Base de données](#a-base-de-données)
    - [B. Serveur Web](#b-serveur-web)
    - [C. Serveur de backup](#c-serveur-de-backup)
    - [D. Reverse Proxy](#d-reverse-proxy)
    - [E. Tableau récap](#e-tableau-récap)



# I. Monitoring

On bouge pas pour le moment niveau machines :

| Machine         | IP            | Service                 | Port ouvert | IPs autorisées |
|-----------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux` | `10.102.1.11` | Serveur Web             | ?           | ?             |
| `db.tp2.linux`  | `10.102.1.12` | Serveur Base de Données | ?           | ?             |



## 2. Setup

🌞 **Setup Netdata**

```bash
[glama@db ~]$ sudo su -
[sudo] password for glama:
[root@db ~]$ bash <(curl -Ss https://my-netdata.io/kickstart-static64.sh)
 --- Downloading static netdata binary: https://storage.googleapis.com/netdata-...
 [...]
 [root@db ~]$ exit
 logout
```

🌞 **Manipulation du *service* Netdata**

```
[glama@db ~]$ systemctl enable netdata
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-unit-files ====
Authentication is required to manage system service or unit files.
Authenticating as: glama
Password:
==== AUTHENTICATION COMPLETE ====
==== AUTHENTICATING FOR org.freedesktop.systemd1.reload-daemon ====
Authentication is required to reload the systemd state.
Authenticating as: glama
Password:
==== AUTHENTICATION COMPLETE ====

[glama@db ~]$ sudo systemctl start netdata


[glama@db ~]$ systemctl status netdata
● netdata.service - Real time performance monitoring
   Loaded: loaded (/usr/lib/systemd/system/netdata.service; enabled; vendor preset: disabled)
   Active: active (running) since Mon 2021-10-11 15:28:53 CEST; 2min 44s ago


[glama@db ~]$ sudo ss -alnpt
State     Recv-Q    Send-Q       Local Address:Port         Peer Address:Port    Process
[...]
LISTEN    0         128                  [::1]:8125                 [::]:*        users:(("netdata",pid=2038,fd=32))
LISTEN    0         128                   [::]:19999                [::]:*        users:(("netdata",pid=2038,fd=6))

### Ajout du port dans le firewall
[glama@db ~]$ sudo firewall-cmd --add-port=19999/tcp
success
[glama@db ~]$ sudo firewall-cmd --add-port=19999/tcp --permanent
success
```


🌞 **Setup Alerting**

```
## Config 
sudo /opt/netdata/etc/netdata/edit-config health_alarm_notify.conf

### Fichier config

###############################################################################
# sending discord notifications

# note: multiple recipients can be given like this:
#                  "CHANNEL1 CHANNEL2 ..."

# enable/disable sending discord notifications
SEND_DISCORD="YES"

# Create a webhook by following the official documentation -
# https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks
DISCORD_WEBHOOK_URL="https://discordapp.com/api/webhooks/XXXXXXXXXXXXX/XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"

# if a role's recipients are not configured, a notification will be send to
# this discord channel (empty = do not send a notification for unconfigured
# roles):
DEFAULT_RECIPIENT_DISCORD="alarms"

```

```
### Test de l'alerte

bash-4.4$ export NETDATA_ALARM_NOTIFY_DEBUG=1
bash-4.4$ /opt/netdata/usr/libexec/netdata/plugins.d/alarm-notify.sh test

# SENDING TEST WARNING ALARM TO ROLE: sysadmin
2021-10-12 15:31:14: alarm-notify.sh: INFO: sent discord notification for: db.tp2.linux test.chart.test_alarm is WARNING to 'alarms'
# OK
[...]

[glama@web ~]$ sudo sed -i 's/curl=""/curl="\/opt\/netdata\/bin\/curl -k"/' /opt/netdata/etc/netdata/health_alarm_notify.conf
[sudo] password for glama:
[glama@web ~]$

```


# II. Backup


## 2. Partage NFS

🌞 **Setup environnement**

```
[glama@backup ~]$ mkdir /srv/backup
mkdir: cannot create directory ‘/srv/backup’: Permission denied

[glama@backup ~]$ sudo mkdir /srv/backup
[sudo] password for glama:

[glama@backup ~]$ cd /srv/backup/

[glama@backup backup]$ sudo mkdir web.tp2.linux

[glama@backup backup]$ ls
web.tp2.linux
``` 

🌞 **Setup partage NFS**

```
### Install nfs
[glama@backup backup]$ sudo dnf -y install nfs-utils

### Config nfs

[glama@backup backup]$ sudo vi /etc/idmapd.conf

#Line 5
Domain = tp2.linux


sudo vi /etc/exports
/srv/backup/web.tp2.linux 10.102.1.11/24(rw,no_root_squash)

[glama@backup backup]$ systemctl enable --now rpcbind nfs-server

[glama@backup backup]$ sudo firewall-cmd --add-service=nfs

```

🌞 **Setup points de montage sur `web.tp2.linux`**

```
[glama@web ~]$ sudo vi /etc/idmapd.conf
# line 5 : uncomment and change to your domain name
Domain = tp2.linux

[glama@web ~]$ sudo mkdir /srv/backup
[glama@web ~]$ sudo mount -t nfs backup.tp2.linux:/srv/backup/web.tp2.linux /srv/backup

[glama@web ~]$ df -hT
Filesystem                                 Type      Size  Used Avail Use% Mounted on
[...]
backup.tp2.linux:/srv/backup/web.tp2.linux nfs4      6.2G  2.4G  3.8G  39% /srv/backup

[glama@web ~]$ mount
[...]
backup.tp2.linux:/srv/backup/web.tp2.linux on /srv/backup type nfs4 (rw,relatime,vers=4.2,rsize=131072,wsize=131072,namlen=255,hard,proto=tcp,timeo=600,retrans=2,sec=sys,clientaddr=10.102.1.11,local_lock=none,addr=10.102.1.13)
```
## 3. Backup de fichiers


🌞 **Rédiger le script de backup `/srv/tp2_backup.sh`**


```bash
$ ./tp2_backup.sh <DESTINATION> <DOSSIER_A_BACKUP>
```

📁 **Fichier `/srv/tp2_backup.sh`**

🌞 **Tester le bon fonctionnement**

```
[glama@web srv]$ sudo ./tp2_backup.sh backup/ toto/
[OK] Archive /srv/hello_211015_111222.tar.gz created.
[OK] Archive /srv/hello_211015_111222.tar.gz synchronized to backup/.
[OK] Directory backup/ cleaned to keep only the 5 most recent backups.
```

```
[glama@web backup]$ sudo tar -xvzf hello_211015_111222.tar.gz
toto/
toto/toto.txt

[glama@web backup]$ cat toto/toto.txt
azer
```

## 4. Unité de service


### A. Unité de service

🌞 **Créer une *unité de service*** pour notre backup

```bash
### Création de la conf systemd
[glama@web ~]$ cd /etc/systemd/system/
[glama@web system]$ sudo vi tp2_backup.service
[sudo] password for glama:

[glama@web system]$ cat tp2_backup.service
[Unit]
Description=Our own lil backup service (TP2)

[Service]
ExecStart=/srv/tp2_backup.sh /srv/ /srv/toto
Type=oneshot
RemainAfterExit=no

[Install]
WantedBy=multi-user.target

[glama@web system]$ sudo mkdir /srv/toto
[glama@web system]$ sudo nano /srv/toto/ttt.txt
```

🌞 **Tester le bon fonctionnement**

```
### Recharge de la conf
[glama@web system]$ sudo systemctl daemon-reload

[glama@web srv]$ ls
backup  toto  tp2_backup.sh

[glama@web srv]$ sudo systemctl start tp2_backup
[glama@web srv]$ ls
backup  hello_211016_111124.tar.gz  toto  tp2_backup.sh
```

---

### B. Timer


🌞 **Créer le *timer* associé à notre `tp2_backup.service`**

```
[glama@web srv]$ cd /etc/systemd/system/

[glama@web system]$ sudo vi tp2_backup.timer

[glama@web system]$ cat tp2_backup.timer
[Unit]
Description=Periodically run our TP2 backup script
Requires=tp2_backup.service

[Timer]
Unit=tp2_backup.service
OnCalendar=*-*-* *:*:00

[Install]
WantedBy=timers.target

```

🌞 **Activez le timer**

```
[glama@web system]$ sudo systemctl start tp2_backup.timer

[glama@web system]$ sudo systemctl enable tp2_backup.timer
Created symlink /etc/systemd/system/timers.target.wants/tp2_backup.timer → /etc/systemd/system/tp2_backup.timer.

[glama@web system]$ sudo systemctl status tp2_backup.timer
● tp2_backup.timer - Periodically run our TP2 backup script
   Loaded: loaded (/etc/systemd/system/tp2_backup.timer; enabled; vendor preset: disabled)
   Active: active (waiting) since Sat 2021-10-16 11:14:52 CEST; 18s ago
  Trigger: Sat 2021-10-16 11:16:00 CEST; 48s left

Oct 16 11:14:52 web.tp2.linux systemd[1]: Started Periodically run our TP2 backup script.


```

🌞 **Tests !**

```bash
[glama@web system]$ cd /srv/

[glama@web srv]$ ls
backup  hello_211016_111124.tar.gz  hello_211016_111452.tar.gz  hello_211016_111502.tar.gz  toto  tp2_backup.sh
```

---

### C. Contexte

🌞 **Faites en sorte que...**

```bash
### Modif dossier a backup
[glama@web system]$ sudo vi tp2_backup.service
[glama@web system]$ cat tp2_backup.service
[Unit]
Description=Our own lil backup service (TP2)

[Service]
ExecStart=/srv/tp2_backup.sh /srv/backup /var/www/sub-domains/web.tp2.linux
Type=oneshot
RemainAfterExit=no

[Install]
WantedBy=multi-user.target

### Modif timer
[glama@web system]$ cat tp2_backup.timer
[Unit]
Description=Periodically run our TP2 backup script
Requires=tp2_backup.service

[Timer]
Unit=tp2_backup.service
OnCalendar=*-*-* 03:15:00

[Install]
WantedBy=timers.target

## Verif 

[glama@web system]$ sudo systemctl list-timers
NEXT                          LEFT     LAST                          PASSED    UNIT                         ACTIVATES
Sun 2021-10-17 03:15:00 CEST  15h left n/a                           n/a       tp2_backup.timer             tp2_backup.service

```

📁 **Fichier `/etc/systemd/system/tp2_backup.timer`**  
📁 **Fichier `/etc/systemd/system/tp2_backup.service`**

## 5. Backup de base de données

Sauvegarder des dossiers c'est bien. Mais sauvegarder aussi les bases de données c'est mieux.

🌞 **Création d'un script `/srv/tp2_backup_db.sh`**

```
### Création d'un user local pour dump la bdd

MariaDB [(none)]> CREATE USER 'savedb'@'localhost' IDENTIFIED BY 'toor';
Query OK, 0 rows affected (0.000 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'savedb'@'localhost';
Query OK, 0 rows affected (0.000 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.000 sec)

### Création script backup 

[glama@db srv]$ sudo cat tp2_backup_db.sh
#!/bin/bash
# Simple backup script database
# glama ~ 16/10/21


backup_name=$(date +hello_%y%m%d_%H%M%S.tar.gz)
backup_fullpath="${backup_name}"


MYSQL_PWD="******" mysqldump -u savedb $2 > $1/db.sql

tar cvzf "$1/${backup_name}" "$1/db.sql" &> /dev/null
rm $1/db.sql

### Test script

[glama@db srv]$ sudo ./tp2_backup.sh /srv/ nextcloud
[glama@db srv]$ ls -l
total 40
drwxr-xr-x. 2 root root     6 Oct 12 17:28 backup
-rw-r--r--. 1 root root 33220 Oct 16 12:52 hello_211016_125210.tar.gz
-rwxr-xr-x. 1 root root   255 Oct 16 12:51 tp2_backup.sh

```

📁 **Fichier `/srv/tp2_backup_db.sh`**  

🌞 **Restauration**

```
[glama@db srv]$ sudo tar -xvzf hello_211016_125210.tar.gz
srv/test.sql

[glama@db srv]$ sudo mysql -u savedb -p****** nextcloud < srv/test.sql

```

🌞 ***Unité de service***

```bash
[glama@db srv]$ cd /etc/systemd/system/
[glama@db system]$ sudo vi tp2_backup_db.service

[glama@db system]$ cat tp2_backup_db.service
[Unit]
Description=Our own lil backup service (TP2)

[Service]
ExecStart=/srv/tp2_backup_db.sh /srv/backup nextcloud
Type=oneshot
RemainAfterExit=no

[Install]
WantedBy=multi-user.target

### TIMER

[glama@db system]$ sudo systemctl start tp2_backup_db.timer
[glama@db system]$ sudo systemctl enable tp2_backup_db.timer
Created symlink /etc/systemd/system/timers.target.wants/tp2_backup_db.timer → /etc/systemd/system/tp2_backup_db.timer.

[glama@db system]$ cat tp2_backup_db.timer
[Unit]
Description=Periodically run our TP2 backup
Requires=tp2_backup_db.service

[Timer]
Unit=tp2_backup_db.service
OnCalendar=*-*-* 03:30:00

[Install]
WantedBy=timers.target

[glama@db system]$ sudo systemctl list-timers
NEXT                          LEFT          LAST                          PASSED       UNIT                         ACTIVATES
Sun 2021-10-17 03:30:00 CEST  14h left      n/a                           n/a          tp2_backup_db.timer          tp2_backup_db.service

[glama@db system]$ sudo systemctl status tp2_backup_db.service
● tp2_backup_db.service - Our own lil backup service (TP2)
   Loaded: loaded (/etc/systemd/system/tp2_backup_db.service; disabled; vendor preset: disabled)
   Active: inactive (dead) since Sat 2021-10-16 13:14:10 CEST; 2min 6s ago
 Main PID: 3568 (code=exited, status=0/SUCCESS)

Oct 16 13:14:10 db.tp2.linux systemd[1]: Starting Our own lil backup service (TP2)...
Oct 16 13:14:10 db.tp2.linux systemd[1]: tp2_backup_db.service: Succeeded.
Oct 16 13:14:10 db.tp2.linux systemd[1]: Started Our own lil backup service (TP2).
[glama@db system]$ sudo systemctl status tp2_backup_db.timer
● tp2_backup_db.timer - Periodically run our TP2 backup
   Loaded: loaded (/etc/systemd/system/tp2_backup_db.timer; enabled; vendor preset: disabled)
   Active: active (waiting) since Sat 2021-10-16 13:14:10 CEST; 2min 17s ago
  Trigger: Sun 2021-10-17 03:30:00 CEST; 14h left

Oct 16 13:14:10 db.tp2.linux systemd[1]: Started Periodically run our TP2 backup.

```

📁 **Fichier `/etc/systemd/system/tp2_backup_db.timer`**  
📁 **Fichier `/etc/systemd/system/tp2_backup_db.service`**


# III. Reverse Proxy


## 2. Setup simple

| Machine            | IP            | Service                 | Port ouvert | IPs autorisées |
|--------------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux`    | `10.102.1.11` | Serveur Web             | ?           | ?             |
| `db.tp2.linux`     | `10.102.1.12` | Serveur Base de Données | ?           | ?             |
| `backup.tp2.linux` | `10.102.1.13` | Serveur de Backup (NFS) | ?           | ?             |
| `front.tp2.linux`  | `10.102.1.14` | Reverse Proxy           | ?           | ?             |

🖥️ **VM `front.tp2.linu`x**

**Déroulez la [📝**checklist**📝](#checklist) sur cette VM.**

🌞 **Installer NGINX**

```
[glama@front ~]$ sudo dnf install nginx
Extra Packages for Enterprise Linux Modular 8 - x86_64                                  505 kB/s | 955 kB     00:01
Extra Packages for Enterprise Linux 8 - x86_64                                          2.3 MB/s |  10 MB     00:04
Last metadata expiration check: 0:00:01 ago on Sat 16 Oct 2021 02:44:29 PM CEST.
Dependencies resolved.

```

🌞 **Tester !**

```
### Start NGINX
[glama@front ~]$ sudo systemctl start nginx
[glama@front ~]$ sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; vendor preset: disabled)
   Active: active (running) since Sat 2021-10-16 14:49:11 CEST; 5s ago
   
[glama@front netdata]$ sudo systemctl enable nginx
Created symlink /etc/systemd/system/multi-user.target.wants/nginx.service → /usr/lib/systemd/system/nginx.service.
   
### port d'écoute

[glama@front ~]$ sudo netstat -tunlp
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name
tcp        0      0 0.0.0.0:80              0.0.0.0:*               LISTEN      4204/nginx: master

## Ouverture firewall 

[glama@front ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success

### Test

C:\Users\guigu> curl front.tp2.linux


StatusCode        : 200
StatusDescription : OK
Content           : <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

                    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
                      <head>
                        <title>Test Page for the Nginx...



```

🌞 **Explorer la conf par défaut de NGINX**

```
### User par défaut

[glama@front ~]$ sudo cat /etc/nginx/nginx.conf
[...]

user nginx;

### Server

 server {
        listen       80 default_server;
        listen       [::]:80 default_server;
        server_name  _;
        root         /usr/share/nginx/html;

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;

        location / {
        }

        error_page 404 /404.html;
            location = /40x.html {
        }

        error_page 500 502 503 504 /50x.html;
            location = /50x.html {
        }
        
### Autre conf

# Settings for a TLS enabled server.
#
#    server {
#        listen       443 ssl http2 default_server;
#        listen       [::]:443 ssl http2 default_server;
#        server_name  _;
#        root         /usr/share/nginx/html;
#
#        ssl_certificate "/etc/pki/nginx/server.crt";
#        ssl_certificate_key "/etc/pki/nginx/private/server.key";
#        ssl_session_cache shared:SSL:1m;
#        ssl_session_timeout  10m;
#        ssl_ciphers PROFILE=SYSTEM;
#        ssl_prefer_server_ciphers on;
#
#        # Load configuration files for the default server block.
#        include /etc/nginx/default.d/*.conf;
#
#        location / {
#        }
#
#        error_page 404 /404.html;
#            location = /40x.html {
#        }
#
#        error_page 500 502 503 504 /50x.html;
#            location = /50x.html {
#        }
#    }




```

🌞 **Modifier la conf de NGINX**

```
[glama@front ~]$ sudo cat /etc/nginx/conf.d/web.tp2.linux.conf
[...]
server {
    # on demande à NGINX d'écouter sur le port 80 pour notre NextCloud
    listen 80;

    # ici, c'est le nom de domaine utilisé pour joindre l'application
    # ce n'est pas le nom du reverse proxy, mais le nom que les clients devront saisir pour atteindre le site
    server_name web.tp2.linux; # ici, c'est le nom de domaine utilisé pour joindre l'application (pas forcéme

    # on définit un comportement quand la personne visite la racine du site (http://web.tp2.linux/)
    location / {
        # on renvoie tout le trafic vers la machine web.tp2.linux
        proxy_pass http://web.tp2.linux;
    }
}
```

## 3. Bonus HTTPS

```bash
[glama@front ~]$ cd ~
[glama@front ~]$ openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout server.key -out server.crt
Generating a RSA private key
.....................+++++
...........................................................+++++
writing new private key to 'server.key'
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [XX]:FR
State or Province Name (full name) []:Aquitaine
Locality Name (eg, city) [Default City]:Bordeaux
Organization Name (eg, company) [Default Company Ltd]:Glama
Organizational Unit Name (eg, section) []:glama
Common Name (eg, your name or your server's hostname) []:web.tp2.linux
Email Address []:glama@glamasite.com
[glama@front ~]$ sudo mv server.key /etc/pki/tls/private/web.tp2.linux.key
[glama@front ~]$ sudo mv server.crt /etc/pki/tls/certs/web.tp2.linux.crt
[glama@front ~]$ sudo chown root:root /etc/pki/tls/private/web.tp2.linux.key
[glama@front ~]$ sudo chown root:root /etc/pki/tls/certs/web.tp2.linux.crt
[glama@front ~]$ sudo chmod 400 /etc/pki/tls/private/web.tp2.linux.key
[glama@front ~]$ sudo chmod 644 /etc/pki/tls/certs/web.tp2.linux.crt
```

🌟 **Modifier la conf de NGINX**

```bash
[glama@front ~]$ cat /etc/nginx/conf.d/web.tp2.linux.conf
server {
    listen 443;

    server_name web.tp2.linux;
    ssl on;
    ssl_certificate     /etc/pki/tls/certs/web.tp2.linux.crt;
    ssl_certificate_key /etc/pki/tls/private/web.tp2.linux.key;

    location / {
         proxy_pass http://web.tp2.linux;
    }
}

### open firewall
  [glama@front ~]$ sudo firewall-cmd --add-port=443/tcp --permanent
  [glama@front ~]$ sudo firewall-cmd --reload

```

🌟 **TEST**

```bash
C:\Users\guigu> curl https://front.tp2.linux


StatusCode        : 200
StatusDescription : OK
Content           : <!DOCTYPE html>
                    <html>
                    <head>
                        <script> window.location.href="index.php"; </script>
                        <meta http-equiv="refresh" content="0; URL=index.php">
```

# IV. Firewalling

## 2. Mise en place

### A. Base de données

🌞 **Restreindre l'accès à la base de données `db.tp2.linux`**

```
[glama@db ~]$ sudo firewall-cmd --permanent --new-zone=db
success
[glama@db ~]$ sudo firewall-cmd --zone=db --add-source=10.102.1.11/32
Error: INVALID_ZONE: db
[glama@db ~]$ sudo firewall-cmd --reload
success
[glama@db ~]$ sudo firewall-cmd --zone=db --add-source=10.102.1.11/32
success
[glama@db ~]$ sudo firewall-cmd --list-all
drop (active)
  target: DROP
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services:
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[glama@db ~]$ sudo firewall-cmd --zone=db --add-port=3306/tcp
success
[glama@db ~]$ sudo firewall-cmd --zone=db --add-port=3306/tcp --permanent
success
[glama@db ~]$ sudo firewall-cmd --zone=db --add-source=10.102.1.11/32 --permanent
success


[glama@db ~]$ sudo firewall-cmd --permanent --new-zone=ssh
success
[glama@db ~]$ sudo firewall-cmd --permanent --zone=ssh --add-source=10.102.1.1/32
[glama@db ~]$ sudo firewall-cmd --zone=ssh --add-port=3306/tcp --permanent
success
[glama@db ~]$ sudo firewall-cmd --reload
success

```

🌞 **Montrez le résultat de votre conf avec une ou plusieurs commandes `firewall-cmd`**

```
[glama@db ~]$ sudo firewall-cmd --get-active-zones
[sudo] password for glama:
db
  sources: 10.102.1.11/32
drop
  interfaces: enp0s8 enp0s3
[glama@db ~]$ sudo firewall-cmd --get-default-zone
drop
[glama@db ~]$ sudo firewall-cmd --list-all --zone=db
db (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.11/32
  services:
  ports: 3306/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

### B. Serveur Web

🌞 **Restreindre l'accès au serveur Web `web.tp2.linux`**

```
[glama@web ~]$ sudo firewall-cmd --permanent --new-zone=reverse_proxy
[sudo] password for glama:
success
[glama@web ~]$ sudo firewall-cmd --permanent --zone=reverse_proxy --add-source=10.102.1.14/32
success
[glama@web ~]$ sudo firewall-cmd --permanent --zone=reverse_proxy --add-port=80/tcp
success
[glama@web ~]$ sudo firewall-cmd --reload
success
[glama@web ~]$ sudo firewall-cmd --permanent --new-zone=ssh
success
[glama@web ~]$ sudo firewall-cmd --permanent --zone=ssh --add-port=22/tcp
success
[glama@web ~]$ sudo firewall-cmd --permanent --zone=ssh --add-source=10.102.1.1/32
success

```

🌞 **Montrez le résultat de votre conf avec une ou plusieurs commandes `firewall-cmd`**

```shell
[glama@web ~]$ sudo firewall-cmd --get-active-zones
drop
  interfaces: enp0s8 enp0s3
reverse_proxy
  sources: 10.102.1.14/32
  
[glama@web ~]$ sudo firewall-cmd --get-default-zone
drop

[glama@web ~]$ sudo firewall-cmd --list-all --zone=reverse_proxy
reverse_proxy (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.14/32
  services:
  ports: 80/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
  
[glama@web ~]$ sudo firewall-cmd --list-all --zone=ssh
Error: INVALID_ZONE: ssh

[glama@web ~]$ sudo firewall-cmd --reload
success

[glama@web ~]$ sudo firewall-cmd --list-all --zone=ssh
ssh (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.1/32
  services:
  ports: 22/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

### C. Serveur de backup

🌞 **Restreindre l'accès au serveur de backup `backup.tp2.linux`**

```
[glama@backup ~]$ sudo firewall-cmd --permanent --new-zone=backup1
[sudo] password for glama:
success
[glama@backup ~]$ sudo firewall-cmd --permanent --new-zone=backup2
success
[glama@backup ~]$ sudo firewall-cmd --zone=backup1 --add-source=10.102.1.11/32
Error: INVALID_ZONE: backup1
[glama@backup ~]$ sudo firewall-cmd --reload
success
[glama@backup ~]$ sudo firewall-cmd --zone=backup1 --add-source=10.102.1.11/32 --permanent
success
[glama@backup ~]$ sudo firewall-cmd --zone=backup2 --add-source=10.102.1.12/32 --permanent
success
[glama@backup ~]$ sudo firewall-cmd --zone=backup2 --add-port=19999/tcp --permanent
success
[glama@backup ~]$ sudo firewall-cmd --zone=backup1 --add-port=19999/tcp --permanent
success
[glama@backup ~]$ sudo firewall-cmd --reload
success
[glama@backup ~]$ sudo firewall-cmd --permanent --new-zone=ssh
success
[glama@backup ~]$ sudo firewall-cmd --reload
success
[glama@backup ~]$ sudo firewall-cmd --permanent --add-source=10.102.1.1/32
success
[glama@backup ~]$ sudo firewall-cmd --permanent --add-port=22/tcp
success
```

🌞 **Montrez le résultat de votre conf avec une ou plusieurs commandes `firewall-cmd`**

```
[glama@backup ~]$ sudo firewall-cmd --get-active-zones
backup1
  sources: 10.102.1.11/32
backup2
  sources: 10.102.1.12/32
drop
  interfaces: enp0s8 enp0s3
ssh
  sources: 10.102.1.1/32
[glama@backup ~]$ sudo firewall-cmd --get-default-zone
drop
  [glama@backup ~]$ sudo firewall-cmd --list-all --zone=backup1
backup1 (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.11/32
  services:
  ports: 19999/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[glama@backup ~]$ sudo firewall-cmd --list-all --zone=backup2
backup2 (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.12/32
  services:
  ports: 19999/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[glama@backup ~]$ sudo firewall-cmd --list-all --zone=ssh
ssh (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.1/32
  services:
  ports: 22/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

### D. Reverse Proxy

🌞 **Restreindre l'accès au reverse proxy `front.tp2.linux`**

```
[glama@front ~]$ sudo firewall-cmd --permanent --new-zone=acces_web
[sudo] password for glama:
success
[glama@front ~]$ sudo firewall-cmd --permanent --zone=acces_web --add-source=10.102.1.0/24
success
[glama@front ~]$ sudo firewall-cmd --permanent --zone=acces_web --add-port=80/tcp
success
[glama@front ~]$ sudo firewall-cmd --permanent --zone=acces_web --add-port=443/tcp
success
[glama@front ~]$ sudo firewall-cmd --reload
success
[glama@front ~]$ sudo firewall-cmd --permanent --new-zone=ssh
success
[glama@front ~]$ sudo firewall-cmd --permanent --zone=ssh --add-source=10.102.1.1/32
success
[glama@front ~]$ sudo firewall-cmd --permanent --zone=ssh --add-port=22/tcp
success
[glama@front ~]$ sudo firewall-cmd --reload
success

```

🌞 **Montrez le résultat de votre conf avec une ou plusieurs commandes `firewall-cmd`**
```
[glama@front ~]$ sudo firewall-cmd --get-active-zones
acces_web
  sources: 10.102.1.0/24
drop
  interfaces: enp0s8 enp0s3
ssh
  sources: 10.102.1.1/32
[glama@front ~]$ sudo firewall-cmd --get-default-zone
drop
[glama@front ~]$ sudo firewall-cmd --list-all --zone=acces_web
acces_web (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.0/24
  services:
  ports: 80/tcp 443/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[glama@front ~]$ sudo firewall-cmd --list-all --zone=ssh
ssh (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.1/32
  services:
  ports: 22/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

### E. Tableau récap

🌞 **Rendez-moi le tableau suivant, correctement rempli :**

| Machine            | IP            | Service                 | Port ouvert | IPs autorisées |
|--------------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux`    | `10.102.1.11` | Serveur Web             | 80/tcp, 19999/tcp       | 10.102.1.14/24             |
| `db.tp2.linux`     | `10.102.1.12` | Serveur Base de Données | 19999/tcp           | 10.102.1.11/24             |
| `backup.tp2.linux` | `10.102.1.13` | Serveur de Backup (NFS) | 19999/tcp           | 10.102.1.11/24 & 10.102.1.12/24|
| `front.tp2.linux`  | `10.102.1.14` | Reverse Proxy           | 443/tcp, 80/tcp, 19999/tcp           | 10.102.1.0/24   |
