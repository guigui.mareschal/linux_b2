#!/bin/bash
# Simple backup script database
# glama ~ 16/10/21


backup_name=$(date +hello_%y%m%d_%H%M%S.tar.gz)
backup_fullpath="${backup_name}"


MYSQL_PWD="toor" mysqldump -u savedb $2 > $1/db.sql

tar cvzf "$1/${backup_name}" "$1/db.sql" &> /dev/null
rm $1/db.sql
